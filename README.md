#run "curl 127.0.0.1:8081/init/" to initiate the DB once you've run the docker-compose.yml file

#Extra functionality:
Supports all base and rate currencies supported by fixer.io
Supports average between currencies
Supports exchange base value of more than 1